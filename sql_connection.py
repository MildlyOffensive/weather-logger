import sqlite3
import json
import requests
import datetime
import logging


def create_sql_structure(weather_data_json):

    try:
        time_stamp = datetime.datetime.now()

        conn = sqlite3.connect('db.sqlite3', detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)

        c = conn.cursor()

        # Create table if none exists yet.
        c.execute('''CREATE TABLE IF NOT EXISTS weather_data
                     (query_id INTEGER PRIMARY KEY NOT NULL, 
                     recorded_date timestamp, 
                     data json);''')

        c.execute("insert into weather_data (recorded_date, data) values (?, ?)", (time_stamp, json.dumps(weather_data_json)))
        conn.commit()

        # Close db connection
        conn.close()

        return time_stamp

    except Exception as e:
        logging.exception("Exception occurred")


if __name__ == '__main__':
    import api_access as api_access

    # Configuration
    url = "https://samples.openweathermap.org/data/2.5/weather"
    query = "London,uk"
    api_key = "b6907d289e10d714a6e88b30761fae22"

    # Set API access settings
    api = api_access.ApiAccess()
    api.url = url
    api.api_key = api_key
    api.query = query

    response = api.query_server().json()

    # create_sql_table_structure_test()
    create_sql_structure(response)
