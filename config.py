# Python 3 script that loads and handles user specified settings from config.ini

from configobj import ConfigObj


def load_settings(location="config.ini"):
    """
    Loads settings from configuration file.
    :param location: file to load configuration from.  Typically "config.ini"
    :return: returns a dictionary containing configuration settings loaded from file.
    """
    # load the configuration file
    config = ConfigObj(location)

    # load the 'IP Configuration' section of the configuration file
    request_config = config['Account Configuration']

    # load the settings from the 'IP Configuration' section
    url = str(request_config['url'])
    query = str(request_config['query'])
    api_key = str(request_config['api_key'])

    # load query time interval settings
    query_interval = int(request_config['query_interval'])
    notification_interval = int(request_config['notification_interval'])

    # return the settings as a dictionary
    settings = {'url': url, 'query': query, 'api_key': api_key, 'query_interval': query_interval,
                'notification_interval': notification_interval}

    # Diagnostic
    print('Settings loaded.')

    return settings


if __name__ == "__main__":
    # test to see that load_settings() works and returns a valid dictionary.
    print(load_settings())
