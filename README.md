# weather-logger

Project to log weather reports from an online API to local storage for analysis and future use.

### Dependencies
- requests
- configobj
- datetime

