import requests
import logging


class ApiAccess:
    """
    Object to manage access to Internet API
    """
    def __init__(self):
        self._url = None
        self._api_key = None
        self._query = None
        self._response = None

    def query_server(self):
        """
        Queries the specified server with a specified query.
        Need to specify all of '_url', '_api_key', and '_query'.
        :return: returns a requests 'Response' object with the response from the server.
        """

        # make sure all values are defined before requesting data from server.
        if self.url is None:
            raise ValueError("url is not defined.")
        if self.api_key is None:
            raise ValueError("api_key is not defined.")
        if self.query is None:
            raise ValueError("query is not defined.")

        # Put all the parts of the query together to send to server.
        complete_query = (self._url + "?q=" + self._query + "&appid=" + self._api_key)

        # Send request to server.
        try:
            request_response = requests.request("GET", complete_query)
            self.response = request_response
        except Exception as e:
            logging.exception("Exception occurred")

        return request_response

    @property
    def url(self):
        return self._url

    @url.setter
    def url(self, value):
        self._url = value

    @property
    def api_key(self):
        return self._api_key

    @api_key.setter
    def api_key(self, value):
        self._api_key = value

    @property
    def query(self):
        return self._query

    @query.setter
    def query(self, value):
        self._query = value

    @property
    def response(self):
        return self._response

    @response.setter
    def response(self, value):
        self._response = value

    @response.deleter
    def response(self):
        del self._response


if __name__ == "__main__":
    # Configuration
    url = "https://samples.openweathermap.org/data/2.5/weather"
    query = "London,uk"
    api_key = "b6907d289e10d714a6e88b30761fae22"

    api_access = ApiAccess()
    api_access.url = url
    api_access.api_key = api_key
    api_access.query = query

    response = api_access.query_server()

    print(response.text)

    # Diagnostic
    print(api_access)
