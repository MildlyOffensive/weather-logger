import api_access as api
import config as config
import sql_connection as sql_conn
import time
import threading
import sys
import logging


"""
Script to retrieve weather data from server and write to SQLite database.
"""

try:

    if __name__ == "__main__":

        # Logging setup.
        logging.basicConfig(level=logging.INFO, filename='app.log', filemode='a',
                            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        logging.info('Program Start')

        # Load settings from config file
        settings = config.load_settings()

        # Set API access settings.
        api_access = api.ApiAccess()
        api_access.url = settings.get('url')
        api_access.api_key = settings.get('api_key')
        api_access.query = settings.get('query')


        class DataLogger(object):
            """

            """

            def __init__(self, interval=600):
                """ Constructor
                :type interval: int
                :param interval: Check interval, in seconds
                """
                self.interval = interval

                thread = threading.Thread(target=self.run, args=())
                thread.daemon = True  # Daemonize thread
                thread.start()  # Start the execution

            def run(self):
                """ Method that runs forever """
                while True:
                    # Query API.
                    response = api_access.query_server().json()

                    # writs SQL data
                    complete_time = sql_conn.create_sql_structure(response)

                    print("Data Retrieved:", complete_time)

                    time.sleep(self.interval)


        # Start logging data.
        try:
            DataLogger(settings.get('query_interval'))
        except Exception as e:
            logging.exception("Exception occurred")

        try:
            while True:
                # Prints time statement to let user know the program is still functioning.
                print("working:", time.strftime("%Y/%m/%d %H:%M:%S"))
                time.sleep(settings.get('notification_interval'))
        except KeyboardInterrupt:
            logging.info('KeyboardInterrupt')
            sys.exit()
        except Exception as e:
            logging.exception("Exception occurred")

except Exception as e:
    logging.exception("Exception occurred")